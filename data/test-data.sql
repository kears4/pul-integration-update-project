--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7
-- Dumped by pg_dump version 11.1

-- Started on 2020-06-05 10:18:34

SET statement_timeout
= 0;
SET lock_timeout
= 0;
SET idle_in_transaction_session_timeout
= 0;
SET client_encoding
= 'UTF8';
SET standard_conforming_strings
= on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies
= false;
SET client_min_messages
= warning;
SET row_security
= off;

--
-- TOC entry 4077 (class 0 OID 31110)
-- Dependencies: 209
-- Data for Name: NUTRIQC_Nutriqc_donnees_A1; Type: TABLE DATA; Schema: data; Owner: ul-pul-dv-obiba-data-p
--

INSERT INTO data."NUTRIQC_Nutriqc_donnees_A1"
VALUES
    ('5852710726', '2020-05-27 10:30:18.119', '2020-06-03 10:53:46.123', NULL, 2, NULL, NULL, NULL, NULL, NULL, 1.69999999999999996, NULL, NULL, 2, NULL, NULL, NULL, NULL, '1', '2', '3', NULL, '0', 0, NULL, '2', NULL, '5', NULL, 0, NULL, 3, 1, '3', NULL, NULL, 0, '2', NULL, NULL, 1, NULL, '3', NULL, NULL, 0, NULL, NULL, 0, '1', NULL, 0, NULL, NULL, NULL, '2', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '1', NULL, '2', NULL, '5', NULL, '1', 2, NULL, 1, '10', NULL, NULL, '0', NULL, '2', NULL, '2', NULL, NULL, NULL, '1', '1', '4', NULL, NULL, 0, '0', NULL, 0, '1', '2', NULL, '2', NULL, NULL, NULL, 0, NULL, NULL, 0, '0', 0, 0, 2, NULL, NULL, '1', NULL, '7', NULL, '1', 2, NULL, '2', NULL, NULL, NULL, '3', '2', NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, 0, '0', NULL, NULL, '1', NULL, NULL, NULL, 135, NULL, 2, '1', '1', NULL, 2, '2', '11', '7', '0', NULL, NULL, NULL, NULL, NULL, 2, '1', NULL, NULL, '2', NULL, 1, NULL, NULL, '1', NULL, NULL, '1', NULL, 2, 1, '3', NULL, NULL, 'h22:m30', '2', '1', '2', NULL, '3', 0, NULL, NULL, NULL, '1', NULL, '3', 0, '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '1', NULL, NULL, NULL, NULL, 2, '3', NULL, NULL, 0, NULL, '3', '0', NULL, 2, '1', '1', '1', NULL, 8, NULL, NULL, NULL, 10, '2', NULL, '0', 0, 0, '3', '5', NULL, '1', 1, NULL, '0', 1, NULL, NULL, '2', NULL, '4', NULL, NULL, NULL, '2', '1', NULL, NULL, NULL, NULL, NULL, '2', NULL, 0, NULL, NULL, NULL, '1', 0, NULL, 'h06:m00', NULL, NULL, '1', NULL, NULL, NULL, 1, 0, '2', NULL, '3', NULL, '2', '3', NULL, 0, NULL, 3, '3', '3', 'Projet', '3', '1', '2', 1, '3', NULL, '2', '2', '5', '3', '2', '4', '2', 3, '4', '2', NULL, '6', '3', '2', NULL, '1', 1, '2', '2', 'INAF', '3', '1', '5', '3', '2', '1', '2', '1', 3, '2', '3', 2, '2', NULL, NULL, '2', NULL, NULL, '2', '3', '3', '3', '2', '1', '1', '2', '6', NULL, 4, 2, '1', '2', '3', NULL, '3', 2, '2', NULL, 2, '2', NULL, '2', '2', '1', '2', NULL, NULL, '3', 'QB-F', 3, '2', '3', '2', '2', 3, '2', '2', '1', 7, '1', NULL, 'NutriQc', '1', '00049', '3', '3', '2', 3, '3', '2', '2', NULL, '1', '3', 'A1', NULL, '2', '2', '2');
INSERT INTO data."NUTRIQC_Nutriqc_donnees_A1"
VALUES
    ('8871003270', '2020-05-27 10:34:35.561', '2020-06-03 10:58:05.203', '1', NULL, NULL, NULL, NULL, NULL, NULL, 1.62999999999999989, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', '2', NULL, '0', NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, 5, NULL, '1', NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, 50, NULL, NULL, '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', NULL, NULL, '2', NULL, '7', NULL, '1', NULL, NULL, NULL, '9', NULL, NULL, '0', NULL, '4', NULL, NULL, NULL, NULL, NULL, '1', NULL, '4', NULL, NULL, NULL, '0', NULL, NULL, NULL, '2', NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '1', NULL, '7', NULL, '1', NULL, NULL, '1', NULL, NULL, NULL, '3', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, NULL, '0', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, '3', '5', '7', '2', NULL, NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, '1', NULL, NULL, NULL, NULL, '1', NULL, NULL, '1', NULL, NULL, NULL, '2', NULL, NULL, 'h20:m00', '2', NULL, '1', NULL, '3', NULL, NULL, NULL, NULL, '4', NULL, '2', NULL, '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, '4', NULL, NULL, NULL, NULL, '3', '0', NULL, NULL, '1', '0', '1', NULL, 8, NULL, NULL, NULL, 5, '2', NULL, '0', NULL, NULL, '2', '5', NULL, '1', NULL, NULL, '0', NULL, NULL, NULL, '2', NULL, '2', NULL, NULL, NULL, '2', '1', NULL, NULL, '6', NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, 'h07:m00', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, 'Nutritionniste', '2', '3', NULL, 1, NULL, 1, NULL, '3', 'Projet', '3', '1', '2', 1, '3', NULL, '2', '2', '5', '2', '2', '4', '2', 4, '4', '2', NULL, '5', NULL, '2', NULL, '1', 1, '2', '2', 'INAF', '3', '2', '5', '3', '2', '1', '2', '2', 4, '2', '3', 4, '2', NULL, NULL, '2', NULL, NULL, '2', '2', '3', '2', '2', '1', '1', '2', '5', NULL, 8, 0, '1', '2', '4', NULL, '3', 0, '2', NULL, 5, '2', NULL, '2', '2', '1', '2', NULL, NULL, '3', 'QB-F', 4, '2', '3', '2', '2', 5, '2', '2', '1', 8, '2', NULL, 'NutriQc', '2', '00035', '3', NULL, '2', 4, '3', '2', '2', NULL, '2', '2', 'A1', NULL, '2', '2', '2');
INSERT INTO data."NUTRIQC_Nutriqc_donnees_A1"
VALUES
    ('5370061121', '2020-05-27 10:34:35.795', '2020-06-03 11:01:32.209', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, 1, NULL, 5, 4, NULL, NULL, NULL, 0, NULL, NULL, NULL, 3, NULL, '2', NULL, NULL, 0, NULL, NULL, 3, NULL, NULL, 2, NULL, '2', NULL, '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 2, NULL, NULL, 3, 2, NULL, NULL, '2', NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, 2, '4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '1', NULL, 3, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, '4', NULL, NULL, 2, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, 3, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 3, NULL, NULL, '2', NULL, NULL, NULL, NULL, 4, NULL, 3, NULL, NULL, 'Projet', NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, 'INAF', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, 2, 5, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, 44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'QB-F', NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, 8, NULL, NULL, 'NutriQc', NULL, '00053', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A1', NULL, NULL, NULL, NULL);


--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7
-- Dumped by pg_dump version 11.1

-- Started on 2020-06-05 13:16:09

SET statement_timeout
= 0;
SET lock_timeout
= 0;
SET idle_in_transaction_session_timeout
= 0;
SET client_encoding
= 'UTF8';
SET standard_conforming_strings
= on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies
= false;
SET client_min_messages
= warning;
SET row_security
= off;

--
-- TOC entry 4077 (class 0 OID 5150241)
-- Dependencies: 281
-- Data for Name: NUTRIQC_Nutriqc_donnees_Web24h1_A1_R1; Type: TABLE DATA; Schema: data; Owner: ul-pul-dv-obiba-data-p
--

INSERT INTO data."NUTRIQC_Nutriqc_donnees_Web24h1_A1_R1"
VALUES
    ('4779393485',
        '2020-06-05 13:10:00.398',
        '2020-06-05 13:10:00.398',
        '"1734";"1603";"1704";"moyenne(1622+1720+1754)";"2933";"2499";"2501";"2380";"2386";"moyenne(2399+4866+4973)";"4848";"2226";"moyenne(1982+2325)";"moyenne(2115+2117+2398+2116+6616)";"2413";"2443";"2598";"422";"214";"4318";"4970";"6195";"2517";"4487";;"2916";"3825";"2933";"1718";"214";"972";"2226";"2381";"2387";"2402";"2414";"2933";"4736";"3393";"3677";"214";"451";"1589";"2242";"2381";"2402";"2422";"2528";"2598";"2605";"2933";"4970";"6673";"2916"',
        '"Ananas,frais/congelé";"Mangue";"Banane";"Jus de fruits,sans sucre ajouté";"Eau du robinet/eau de source/eau gazéifiée";"Algue séchée,poudre";"Betterave,fraîche/congelée";"Carotte,fraîche/congelée,crue";"Céleri,cru";"Champignon,frais";"Concombre,pelé";"Courgette/zucchini";"Germe (luzerne,radis,moutarde,brocoli)";"Mélange de laitues";"Poivron,frais/congelé,cru";"Radis";"Beurre de sésame/tahini";;;;;;"Graines,de citrouille/courge,non salées";"Sarrasin";"Non,le sarrasin n''était pas cuit dans du bouillon/consommé/fumet";"Tisane";"Biscuit au gingembre";"Eau du robinet/eau de source/eau gazéifiée";"Raisin";;;;;;;;;;"Lentille sèche,bouillie";"Bagel,autres types (avoine/lin/kamut/orge/sarrazin/seigle/son,etc.)";;;;;;;;;;;;"Moutarde préparée/moutarde de dijon";"Chocolat,noir,sans noix";"Tisane"',
        '343.76;517.5;437.78;255.02;0.0;18.14;79.12;53.63;20.77;38.88;15.6;29.44;24.84;20.77;31.92;19.14;355.6;802.21;0.0;3.1;3.01;5.4;336.14;1291.5;;10.0;121.87;0.0;288.0;0.0;52.8;21.11;28.41;12.44;36.34;38.59;0.0;227.31;243.5;960.3;0.0;50.39;0.68;11.6;11.63;4.15;11.67;44.01;72.09;81.42;0.0;12.65;460.2;10.0'
   );