-- FUNCTION: "data-staging".get_matching_tables(text, text, text)

-- DROP FUNCTION "data-staging".get_matching_tables(text, text, text);

CREATE OR REPLACE FUNCTION "data-staging".get_matching_tables
(
	p_schema_name text,
	p_project_key text,
	p_table_group text)
    RETURNS text[]
    LANGUAGE 'sql'

    COST 100
    VOLATILE 
AS $BODY$
select array_agg(t.table_name::text
order by t.table_name
)
        from   information_schema.tables t
        where  t.table_schema   = p_schema_name
        and    t.table_name     ilike concat
(p_project_key, '_', p_table_group, '%');
$BODY$;
