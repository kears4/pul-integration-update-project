-- FUNCTION: "data-staging".update_project(text, text, text, text, boolean)

-- DROP FUNCTION "data-staging".update_project(text, text, text, text, boolean);

CREATE OR REPLACE FUNCTION "data-staging".update_project_split_rows(
	p_src_schema_name text,
	p_src_project_key text,
	p_src_table_group text,
	p_dst_schema_name text)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE SECURITY DEFINER 
    SET search_path='"data-staging", public, pg_temp'
AS $BODY$
declare
    c_stg_schema         constant text     := p_src_schema_name || '-staging';
    c_stg_table          constant text     := lower(p_src_project_key || '_' || p_src_table_group);
	-- claudia : décommenter 1ere ligne et supprimer 2e
    --c_stg_table_pk_cols  constant text[]   := array['opal_table', 'opal_id', 'visites', 'rappel'];
    c_stg_table_pk_cols  constant text[]   := array['opal_table', 'opal_id'];

    c_dst_view           constant text     := lower(p_src_table_group);         -- Name of the view in the dst schema
    c_dst_view_cols_excl constant text[]   := array['opal_id',                  -- Excluded columns from final view
                                                    'opal_table',
                                                    'opal_created',
                                                    'opal_updated'];

    l_src_tables         text[];    -- Source tables that matches «project key» and «table group»
    l_src_tables_cols    text[];    -- Common columns for matched tables

    l_stg_table_cols     text[];    -- Current staging table columns
    l_stg_table_selects  text[];    -- Use to store list of each constructed selects, to be joined by UNION ALL
    l_stg_src_query      text;      -- Use to store the final source query (union of all select parts)

    l_dst_view_cols      text[];    -- Final list of columns to include in final view

    l_current_table      text;      -- To hold current table ame while looping
    l_current_column     text;      -- To hold current column name while looping
    l_stmt               text;      -- Working and temporary variable to hold various statements building

    l_nb_rows            integer;
begin
    -------------
    -- 1. Get matching source tables
    -------------

    l_src_tables := "data-staging".get_matching_tables(p_schema_name   => p_src_schema_name,
                                        p_project_key   => p_src_project_key,
                                        p_table_group   => p_src_table_group);

    if    l_src_tables is null
       or cardinality(l_src_tables) = 0
    then
        raise exception 'No matching table found.';
    end if;

    raise notice 'Matched tables: %', l_src_tables;

    -------------
    -- 2. Get maching source tables and their common columns
    -------------

    select "data-staging".array_intersect_ordered_agg(table_cols) as common_cols
    into   l_src_tables_cols
    from   (
                -- Get one line per table, with columns in an array
		        select c.table_name,
                       array_agg(c.column_name::text order by c.ordinal_position) as table_cols
                from   
				      (select c.table_name,
                                case when c.column_name IN ('opal_id', 'opal_created', 'opal_updated') THEN c.column_name
				  		   		else 	case when v.value_type = 'text' then concat('unnest(string_to_array(', c.column_name ,', '';''))', ' as ', c.column_name)
										     when v.value_type = 'integer' then concat('cast(nullif(unnest(string_to_array(', c.column_name ,', '';'')), '''') as integer)', ' as ', c.column_name)
											 when v.value_type = 'decimal' then concat('cast(nullif(unnest(string_to_array(', c.column_name ,', '';'')), '''') as double precision)', ' as ', c.column_name)
						                     when v.value_type = 'date' then concat('cast(nullif(unnest(string_to_array(', c.column_name ,', '';'')), '''') as date)', ' as ', c.column_name)
                                		end
								end as column_name,                              
								c.ordinal_position
						from information_schema.columns c
                        LEFT OUTER JOIN data.variables v ON (v.datasource = substring(c.table_name,0,8) 
															and v.value_table = substring(c.table_name, 9) 
															and v.name = c.column_name)
                		where  c.table_schema = p_src_schema_name
                		and    c.table_name   = any(l_src_tables)
					   and c.column_name IN ('opal_table', 'opal_id', 'opal_created', 'opal_updated', 'code_fcen', 'aliment_id', 'kj')
					 ) as c
                group by c.table_name
           ) as t;

    if    l_src_tables_cols is null
       or cardinality(l_src_tables_cols) = 0
    then
        raise exception 'No common columns in matched tables list.';
    end if;

    raise notice 'Common columns in matched tables: %', l_src_tables_cols;

    -------------
    -- 3. Generate select statement to feed the rest of the pipeline (base tables --> staging)
    -------------

    foreach l_current_table in array l_src_tables
    loop
        l_stmt := 'SELECT ' || quote_literal(l_current_table)                               || ' AS opal_table, '
                            || array_to_string(l_src_tables_cols, ', ')  || ' ' ||
                  'FROM '   || quote_ident(p_src_schema_name) || '.' || quote_ident(l_current_table);

        l_stg_table_selects := array_append(l_stg_table_selects, l_stmt);
    end loop;

    l_stg_src_query := array_to_string(l_stg_table_selects, E'\nUNION ALL\n');

    -------------
    -- 4. Get staging table columns
    -------------

    l_stg_table_cols := "data-staging".get_table_columns(c_stg_schema, c_stg_table);
    raise notice 'Current staging table columns: %', l_stg_table_cols;

    -------------
    -- 5. Stage new table
    -------------

    raise info 'Staging table is not present or with different columns, droping and re-creating...';

    l_stmt := 'DROP TABLE IF EXISTS ' || quote_ident(c_stg_schema) || '.' || quote_ident(c_stg_table) || ' CASCADE';
    raise notice '%', l_stmt;
    execute l_stmt;

    l_stmt := 'CREATE TABLE ' || quote_ident(c_stg_schema) || '.' || quote_ident(c_stg_table) || E' AS\n' || l_stg_src_query;
    raise notice '%', l_stmt;
    execute l_stmt;

    get diagnostics l_nb_rows := ROW_COUNT;
    raise notice 'Table created and initalized with % rows', l_nb_rows;

    -- claudia : à décommenter lorsque nous aurons toutes les colonnes de la clé primaire + données véridiques
    --l_stmt := 'ALTER TABLE ' || quote_ident(c_stg_schema) || '.' || quote_ident(c_stg_table) || ' ADD PRIMARY KEY (' || array_to_string(c_stg_table_pk_cols, ',') || ')';
    --raise notice '%', l_stmt;
    --execute l_stmt;

    -- Refresh staging table columns
    l_stg_table_cols := "data-staging".get_table_columns(c_stg_schema, c_stg_table);
    raise notice 'Current staging table columns: %', l_stg_table_cols;

    -- Remove excluded columns
    l_dst_view_cols := l_stg_table_cols;
    foreach l_current_column in array c_dst_view_cols_excl
    loop
        l_dst_view_cols := array_remove(l_dst_view_cols, l_current_column);
    end loop;
                                                                    
    l_stmt := 'DROP VIEW IF EXISTS ' || quote_ident(p_dst_schema_name) || '.' || quote_ident(c_dst_view);
    execute l_stmt;

    -- Generate final view
    l_stmt := 'CREATE OR REPLACE VIEW ' || quote_ident(p_dst_schema_name) || '.' || quote_ident(c_dst_view) || E'\n\t' ||
                    'WITH (SECURITY_BARRIER = TRUE) AS'                                                     || E'\n\t' ||
                    'SELECT ' || array_to_string("data-staging".array_quote_ident(l_dst_view_cols), ',')                   || E'\n\t' ||
                    'FROM   ' || quote_ident(c_stg_schema) || '.' || quote_ident(c_stg_table);
    raise notice '%', l_stmt;
    execute l_stmt;

    return l_nb_rows;
end;
$BODY$;