CREATE OR REPLACE FUNCTION "data-staging".get_table_columns(p_schema_name text,p_table_name text,p_table_type text DEFAULT 'BASE TABLE'::text)
    RETURNS text[]
    LANGUAGE 'sql'
    VOLATILE
    PARALLEL UNSAFE
    COST 100
AS $BODY$    select array_agg(c.column_name::text order by c.ordinal_position)
    from   information_schema.tables t
                inner join information_schema.columns c
                on     t.table_schema = c.table_schema
                   and t.table_name   = c.table_name
    where  t.table_type   = p_table_type
    and    c.table_schema = p_schema_name
    and    c.table_name   = p_table_name;
$BODY$;