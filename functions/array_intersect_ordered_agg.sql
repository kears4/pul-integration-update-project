-- FUNCTION: "data-staging".array_intersect_ordered_agg(text[], text[])

-- DROP FUNCTION "data-staging".array_intersect_ordered_agg
-- (text[],text[])

CREATE OR REPLACE FUNCTION "data-staging".array_intersect_ordered_agg
(
	p_a1 text[],
	p_a2 text[] DEFAULT NULL)
    RETURNS text[]
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
declare
begin
    return "data-staging".array_intersect_agg(p_a1, p_a2, true);
end;
$BODY$;

