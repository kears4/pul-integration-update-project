-- FUNCTION: "data-staging".array_quote_ident(anyarray)

-- DROP FUNCTION "data-staging".array_quote_ident(anyarray);

CREATE OR REPLACE FUNCTION "data-staging".array_quote_ident
(
	anyarray)
    RETURNS text[]
    LANGUAGE 'sql'

    COST 100
    VOLATILE 
AS $BODY$
select array_agg(quote_ident(elem))
from unnest($1) as t(elem);
$BODY$;

