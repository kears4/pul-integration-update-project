-- FUNCTION: "data-staging".array_intersect_agg(text[], text[], boolean)

-- DROP FUNCTION "data-staging".array_intersect_agg(text[], text[], boolean);

CREATE OR REPLACE FUNCTION "data-staging".array_intersect_agg
(
	p_a1 text[],
	p_a2 text[],
	p_ordered boolean)
    RETURNS text[]
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
declare
    ret text[];
begin
    -- If one side is null, simply return the other

    if p_a1 is null
    then
    return p_a2;
    elseif p_a2 is null
    then
    return p_a1;
end
if;

    -- Use SQL to do the intersect

    select array_agg(e
order by case when p_ordered then array_position(p_a1, e) else null end,
                                case when p_ordered then array_position(p_a2, e) else null end
)
    into ret
    from
    (
        select unnest(p_a1)
intersect
    select unnest(p_a2)
    )
as dt
(e);

return ret;
end;
$BODY$;

