-- FUNCTION: "data-staging".array_sort(anyarray)

-- DROP FUNCTION "data-staging".array_sort(anyarray);

CREATE OR REPLACE FUNCTION "data-staging".array_sort
(
	anyarray)
    RETURNS anyarray
    LANGUAGE 'sql'

    COST 100
    VOLATILE 
AS $BODY$
select array(
select unnest($1)
order by 1
)
$BODY$;

