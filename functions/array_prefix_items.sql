-- FUNCTION: "data-staging".array_prefix_items(anyarray, text)

-- DROP FUNCTION "data-staging".array_prefix_items(anyarray, text);

CREATE OR REPLACE FUNCTION "data-staging".array_prefix_items
(
	anyarray,
	text)
    RETURNS text[]
    LANGUAGE 'sql'

    COST 100
    VOLATILE 
AS $BODY$
select array_agg($2 || elem)
from unnest($1) as t(elem);
$BODY$;

