--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7
-- Dumped by pg_dump version 11.1

-- Started on 2020-06-15 09:48:25

SET statement_timeout
= 0;
SET lock_timeout
= 0;
SET idle_in_transaction_session_timeout
= 0;
SET client_encoding
= 'UTF8';
SET standard_conforming_strings
= on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies
= false;
SET client_min_messages
= warning;
SET row_security
= off;

SET default_tablespace
= '';

SET default_with_oids
= false;

--
-- TOC entry 238 (class 1259 OID 31307)
-- Name: variables; Type: TABLE; Schema: data; Owner: ul-pul-dv-obiba-data-p
--

CREATE TABLE data.variables
(
    datasource character varying(255) NOT NULL,
    value_table character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    value_type character varying(255) NOT NULL,
    ref_entity_type character varying(255),
    mime_type character varying(255),
    units character varying(255),
    is_repeatable boolean,
    occurrence_group character varying(255),
    index integer,
    sql_name character varying(255) NOT NULL
);

--
-- TOC entry 3961 (class 2606 OID 31872)
-- Name: variables VARIABLES_PKEY; Type: CONSTRAINT; Schema: data; Owner: ul-pul-dv-obiba-data-p
--

ALTER TABLE ONLY data.variables
ADD CONSTRAINT "VARIABLES_PKEY" PRIMARY KEY
(datasource, value_table, name);


--
-- TOC entry 4102 (class 0 OID 0)
-- Dependencies: 238
-- Name: TABLE variables; Type: ACL; Schema: data; Owner: ul-pul-dv-obiba-data-p
--


-- Completed on 2020-06-15 09:48:29

--
-- PostgreSQL database dump complete
--

