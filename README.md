## Partir serveur postgres et pgadmin
.\build.cmd
(Effectuer la commande dans le dossier racine du repository)

## Se connecter à la base de données dans pgadmin
1. Ouvrir pgadmin dans navigateur: http://localhost:5555/
2. Se connectecter avec user **pgadmin4@pgadmin.org** et password **admin**
3. Créer une connection au server avec les info suivantes:

Host: host.docker.internal
Port: 6543
Maintenance database: defaultdb
Username: postgres
Password: postgres

## Scénario
Un scénario de données est généré avec le fichier sr-keymaster-db > data > test-data.sql
Ce scénario attribue les permissions de select/update/delete via le rôle ul-val-dv-p de la base de données ul-val-dv créer dans ce docker-compose.
Le backend sr-keymaster en mode local se connect sur la bd hosté dans docker et exécute ce scénario.
